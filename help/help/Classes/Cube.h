#pragma once

#include "Object.h"


namespace pndev {

	class Cube : public Object
	{
	public:
		Cube(ShaderProgram* shader);
		~Cube();

		void update();
		void initialize();

	private:

		GLuint vao;

	protected:

		void renderInternal();

	};

}