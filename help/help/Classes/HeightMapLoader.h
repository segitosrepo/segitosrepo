#ifndef HEIGHTMAP_H
#define HEIGHTMAP_H

#include <iostream>
#include <vector>

#include "GLIncludes.h"
#include <GLM\glm.hpp>
#include <GLM\gtc\matrix_transform.hpp>

#include "pndtexture.h"
#include "ObjectLoader.h"

using namespace std;

namespace ti 
{
	class HeightMap : public Object
	{
	public:

		HeightMap(Shader& shader);

		GLuint
			terrainVertexArrayBuffer,
			mapCoordinateBufferID,
			mapIndexBufferID,
			mapNormalBufferID;

		void loadHeightmap(const char*, float);

		void initHeightmap(const char*, float);
		void renderHeightmap();

		float heightScale;
		void getVertexNormal();

		std::vector<float> coords;
		std::vector<unsigned int> mapindex;
		std::vector<float> mapnormal;

		unsigned int indexRange;
		unsigned int indexRows;

		PNDTEXTURE terr_texture1;
		PNDTEXTURE terr_texture2;
		PNDTEXTURE terr_texture3;
		PNDTEXTURE terr_shore;

		int mapwidth;
		int mapheight;
		long image_size;
		unsigned char* data;

		glm::vec3 getCoordData(int, int);

		void initialize();
		void update();		
		void renderInternal();
	};
#endif
}