#include "Object.h"

int pndev::Object::ID_AUTO_INCR_ = 0;

pndev::Object::Object(ShaderProgram* shader):
	shader_(shader),
	id_(ID_AUTO_INCR_),
	position_(glm::vec3(0, 0, 0)),
	rotation_(glm::quat()),
	scale_(glm::vec3(1, 1, 1))

{
};

void pndev::Object::setRotation(const glm::quat& quaternion)
{
	rotation_ = quaternion;
	rotation_ = glm::normalize(rotation_);
};

void pndev::Object::setRotation(float x, float y, float z, float angle)
{
	rotation_ = glm::angleAxis(glm::radians(angle), glm::vec3(x, y, z));
};

void pndev::Object::setPosition(float x, float y, float z)
{
	position_.x = x;
	position_.y = y;
	position_.z = z;
};

void pndev::Object::setScale(float x, float y, float z)
{
	scale_.x = x;
	scale_.y = y;
	scale_.z = z;
}
void pndev::Object::setShader(ShaderProgram * shader)
{
	shader_ = shader;
}

glm::quat pndev::Object::getRotation()
{
	return rotation_;
};

glm::vec3 pndev::Object::getPosition()
{
	return position_;
}

glm::vec3 pndev::Object::getScale()
{
	return scale_;
}

/*pndev::BoundingBox* pndev::Object::getBoundingBox()
{
	if (bounding_box_ == nullptr) return nullptr;
	transformed_aabb_ = bounding_box_->getTransformedByModelMatrix(getModelMatrix());
	return &transformed_aabb_;
}*/

pndev::ShaderProgram* pndev::Object::getShader()
{
	return shader_;
}

glm::mat4 pndev::Object::getModelMatrix()
{
	glm::mat4 S = glm::scale(glm::mat4(), scale_);
	S[3][3] = 1;
	glm::mat4 R = glm::toMat4(rotation_);
	glm::mat4 invR = glm::transpose(R);
	glm::vec4 t = glm::vec4(position_.x, position_.y, position_.z, 1)*invR;
	glm::vec3 t3(t.x, t.y, t.z);
	return glm::translate(R*S, t3);
};

void pndev::Object::render(ti::Camera* camera, GLuint shadowmap, const glm::mat4& lightMatrix)
{
	if (shader_ != nullptr && camera != nullptr)
	{
		shader_->use();

		GLuint mmu = shader_->getUniformLocation("model_matrix");
		GLuint vmu = shader_->getUniformLocation("view_matrix");
		GLuint pmu = shader_->getUniformLocation("projection_matrix");

		glUniformMatrix4fv(mmu, 1, GL_FALSE, glm::value_ptr(getModelMatrix()));
		glUniformMatrix4fv(vmu, 1, GL_FALSE, glm::value_ptr(camera->getViewMatrix()));
		glUniformMatrix4fv(pmu, 1, GL_FALSE, glm::value_ptr(camera->getProjectionMatrix()));

		if (shadowmap != -1) 
		{
			GLuint lmu = shader_->getUniformLocation("light_vp");
			GLuint shu = shader_->getUniformLocation("depth_texture");
			glUniformMatrix4fv(lmu, 1, GL_FALSE, glm::value_ptr(lightMatrix));
			glUniform1i(shu, 0);

			glActiveTexture(GL_TEXTURE0);
			glBindTexture(GL_TEXTURE_2D, shadowmap);
		}
		renderInternal();
	}
}

void pndev::Object::render(ShaderProgram* customShader, const glm::mat4& view, const glm::mat4& projection)
{
	if (customShader != nullptr)
	{
		customShader->use();

		GLuint mmu = glGetUniformLocation(customShader->getProgram(), "model_matrix");
		GLuint vmu = glGetUniformLocation(customShader->getProgram(), "view_matrix");
		GLuint pmu = glGetUniformLocation(customShader->getProgram(), "projection_matrix");

		glUniformMatrix4fv(mmu, 1, GL_FALSE, glm::value_ptr(getModelMatrix()));
		glUniformMatrix4fv(vmu, 1, GL_FALSE, glm::value_ptr(view));
		glUniformMatrix4fv(pmu, 1, GL_FALSE, glm::value_ptr(projection));

		renderInternal();
	}
}

