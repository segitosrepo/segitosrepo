#include "RenderScene.h"
#include "ObjectLoader.h"

ti::RenderScene* ti::RenderScene::active_camerascene_ = nullptr;

ti::RenderScene* ti::RenderScene::getActiveScene()
{
	return active_camerascene_;
}

ti::RenderScene::RenderScene(std::string name) :
	scene_name_(name),
	active_camera_(nullptr)
{
	if (active_camerascene_ == nullptr)
		active_camerascene_ = this;
}

ti::RenderScene::~RenderScene()
{
	if (active_camerascene_ == this)
		active_camerascene_ = nullptr;
}

void ti::RenderScene::initialize(const ti::SceneSettings& settings)
{
	active_camerascene_ = this;
}

void ti::RenderScene::update()
{
	for (auto object : objects_) 
	{
		object->update();
	}
}

void ti::RenderScene::render()
{
	if (active_camera_ == nullptr)
	{
		return;
	}

	glClear(GL_COLOR_BUFFER_BIT | GL_DEPTH_BUFFER_BIT);

	 for (auto object : objects_)
	 {
		 object->render(active_camera_, -1, glm::mat4());
	 }

	 water->waterRefraction.bindFBO();
	 for (auto object : objects_)
	 {
		 object->render(active_camera_, -1, glm::mat4());
	 }
	 FBOMANAGER::unbindFBO();

	 water->waterReflection.bindFBO();
	 for (auto object : objects_)
	 {
		 object->render(object->getShader(), active_camera_->getViewMatrix(), active_camera_->getProjectionMatrix());
	 }
	 FBOMANAGER::unbindFBO();

	 water->render(active_camera_, -1, glm::mat4());
}

void ti::RenderScene::setActiveCamera(int index)
{
	if (index >= 0 && index < cameras_.size())
	{
		active_camera_ = cameras_[index];
	}
}

void ti::RenderScene::setWater(ti::WATER * water)
{
	this->water = water;
	this->water->initialize();
}

void ti::RenderScene::addCamera(ti::Camera* camera)
{
	if (camera != nullptr)
	{
		cameras_.push_back(camera);

		if (active_camera_ == nullptr)
		{
			active_camera_ = camera;
		}
	}
}

void ti::RenderScene::addObject(ti::Object* obj)
{
	if (obj != nullptr)
	{
		obj->initialize();
		objects_.push_back(obj);
	}
}

ti::Camera* ti::RenderScene::getActiveCamera()
{
	return active_camera_;
}

ti::Camera* ti::RenderScene::getCamera(int index)
{
	if (index < cameras_.size() && index >= 0)
		return cameras_[index];
	return nullptr;
}

std::vector<ti::Camera*> ti::RenderScene::getCameras()
{
	return cameras_;
}
