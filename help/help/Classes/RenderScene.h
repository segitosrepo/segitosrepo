#pragma once
#include <vector>
#include <string>

#include "Camera.h"
#include "ObjectLoader.h"
#include "Water.h"

namespace ti
{
	struct SceneSettings
	{
		glm::ivec2 shadowmapResolution;
	};

	/*
		A scene that holds the objects and gui to be rendered.
		Supports light, shadows, skybox, multiple cameras.
	*/
	class RenderScene
	{
	public:

		/*
			Retrieves the current active scene. It is used for rendering.
		*/
		static RenderScene* getActiveScene();

		RenderScene(std::string name);
		~RenderScene();

		void initialize(const SceneSettings& settings);

		void update();
		void render();

		void setActiveCamera(int cameraIndex);
		void setWater(ti::WATER* water);
		void addCamera(ti::Camera* camera);
		void addObject(ti::Object* obj);

		ti::Camera* getActiveCamera();
		ti::Camera* getCamera(int index);
		std::vector<ti::Camera*> getCameras();

	private:

		static RenderScene* active_camerascene_;

		ti::Camera* active_camera_;

		WATER* water;

		std::string scene_name_;
		std::vector<ti::Object*> objects_;
		std::vector<ti::Camera*> cameras_;
	};
}