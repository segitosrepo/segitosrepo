#include "Scene.h"

ti::Scene* ti::Scene::active_scene_ = nullptr;

ti::Scene* ti::Scene::getActiveScene()
{
	return active_scene_;
}

ti::Scene::Scene(std::string name) : 
	scene_name_(name),
	// skybox_(nullptr),
	active_camera_(nullptr)
{
	if (active_scene_ == nullptr)
		active_scene_ = this;
}

ti::Scene::~Scene()
{
	if (active_scene_ == this)
		active_scene_ = nullptr;
}

void ti::Scene::initialize(const ti::SceneSettings& settings)
{
	active_scene_ = this;
	// shadow_map_.initialize(settings.shadowmapResolution.x, settings.shadowmapResolution.y);
}

void ti::Scene::update()
{
	for (auto object : objects_) 
	{
		object->update();
	}
}

void ti::Scene::render()
{
	if (active_camera_ == nullptr)
	{
		return;
	}

	glm::mat4 lightVP;

	// if (light_ != nullptr && light_->castShadows())
	// {
	// 	lightVP = shadow_map_.computeShadowmap(light_, objects_);
	// }

	glClear(GL_COLOR_BUFFER_BIT | GL_DEPTH_BUFFER_BIT);

	// if (skybox_ != nullptr) skybox_->renderSkybox(active_camera_);

	 for (auto object : objects_)
	 {
	 	//light_->sendLight(object->getShader());
	 	object->render(active_camera_, -1, lightVP);
	 }

	for (auto elem : gui_elements_)
	{
		elem->render(active_camera_);
	}
}

void ti::Scene::setActiveCamera(int index)
{
	if (index >= 0 && index < cameras_.size())
	{
		active_camera_ = cameras_[index];
	}
}

void ti::Scene::addCamera(ti::Camera* camera)
{
	if (camera != nullptr)
	{
		cameras_.push_back(camera);

		if (active_camera_ == nullptr)
		{
			active_camera_ = camera;
		}
	}
}

void ti::Scene::addObject(pndev::Object* obj)
{
	if (obj != nullptr)
	{
		obj->initialize();
		objects_.push_back(obj);
	}
}

void ti::Scene::addGui(pndev::GuiElement* gui)
{
	if (gui != nullptr)
	{
		gui_elements_.push_back(gui);
	}
}

ti::Camera* ti::Scene::getActiveCamera()
{
	return active_camera_;
}

ti::Camera* ti::Scene::getCamera(int index)
{
	if (index < cameras_.size() && index >= 0)
		return cameras_[index];
	return nullptr;
}

std::vector<ti::Camera*> ti::Scene::getCameras()
{
	return cameras_;
}

// void ti::Scene::setSkybox(SkyBox* skybox)
// {
// 	this->skybox_ = skybox;
// }

// ti::SkyBox* ti::Scene::getSkybox()
// {
// 	return skybox_;
// }
// 
// void ti::Scene::setLight(DirectionalLight* light)
// {
// 	this->light_ = light;
// }
// 
// ti::DirectionalLight* ti::Scene::getLight()
// {
// 	return light_;
// }
