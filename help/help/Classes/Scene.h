#pragma once
#include <vector>
#include <string>

#include "Camera.h"
// #include "SkyBox.h"
// #include "DirectionalLight.h"
#include "Object.h"
#include "GUIElement.h"
// #include "Shadowmap.h"

namespace ti
{
	struct SceneSettings
	{
		glm::ivec2 shadowmapResolution;
	};

	/*
		A scene that holds the objects and gui to be rendered.
		Supports light, shadows, skybox, multiple cameras.
	*/
	class Scene
	{
	public:

		/*
			Retrieves the current active scene. It is used for rendering.
		*/
		static Scene* getActiveScene();

		Scene(std::string name);
		~Scene();

		void initialize(const SceneSettings& settings);

		void update();
		void render();

		void setActiveCamera(int cameraIndex);
		void addCamera(ti::Camera* camera);
		void addObject(pndev::Object* obj);
		void addGui(pndev::GuiElement* gui);

		ti::Camera* getActiveCamera();
		ti::Camera* getCamera(int index);
		std::vector<ti::Camera*> getCameras();

		// void setSkybox(SkyBox* skybox);
		// SkyBox* getSkybox();

		// void setLight(DirectionalLight* light);
		// DirectionalLight* getLight();

	private:

		static Scene* active_scene_;

		ti::Camera* active_camera_;

		std::string scene_name_;
		std::vector<pndev::Object*> objects_;
		std::vector<pndev::GuiElement*> gui_elements_;
		std::vector<ti::Camera*> cameras_;

		// SkyBox* skybox_;
		// DirectionalLight* light_;
		// Shadowmap shadow_map_;
	};
}