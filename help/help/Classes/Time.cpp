#include "Time.h"

#include <GLFW\glfw3.h>

static double elapsedTime = 0;
static double deltaTime = 0;

static double frameStart = 0;

double pndev::Time::getFrameDelta()
{
	return deltaTime;
}

double pndev::Time::getElapsedTime()
{
	return glfwGetTime();
}

void pndev::Time::init()
{
	frameStart = glfwGetTime();
}

void pndev::Time::calcFrameTime()
{
	double cur = glfwGetTime();
	deltaTime = cur - frameStart;
	frameStart = cur;
}

pndev::Time::Time()
{
}


pndev::Time::~Time()
{
}
