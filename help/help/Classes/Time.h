#pragma once

namespace pndev {

	class Time
	{
	public:
		static double getFrameDelta();
		static double getElapsedTime();

		static void init();
		static void calcFrameTime();

	private:
		Time();
		~Time();
	};

}