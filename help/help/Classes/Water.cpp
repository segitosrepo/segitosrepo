#include "Water.h"
#include <iostream>
#include <fstream>

#include "TimeCalculator.h"

ti::WATER::WATER(Shader& shader, int w, int h) : 
	Object(&shader),
	width(w),
	height(h)
{
}

void ti::WATER::loadWater()
{
	for(int rows=0;rows<height;rows++)
		for(int cols=0;cols<width;cols++)
		{
			watercoords.push_back(cols);
	        watercoords.push_back(0);
			watercoords.push_back(rows);
		}

	for(int row=0;row<height-1;row++)
	{
		for(int col=0;col<width-1;col++)
		{
			waterindex.push_back(row*width+col);
			waterindex.push_back((row+1)*width+col);
			waterindex.push_back(row*width+col+1);

			waterindex.push_back(row*width+col+1);
            waterindex.push_back((row+1)*width+col);
			waterindex.push_back((row+1)*width+col+1);
		}
	}

	indexRange=6*width;
	indexRows=height-1;
};

void ti::WATER::init_water()
{
	waterlevel=10.0f;
	loadWater();

	normal_map.loadteximage("Texture/Water/normal.BMP");
	foam.loadteximage("Texture/Water/foam.BMP");

	glGenBuffers(1,&waterCoordBufferID);
	glGenBuffers(1,&waterIndexBufferID);

	glGenVertexArrays(1,&waterVertArrayBuffer);

	glBindBuffer(GL_ELEMENT_ARRAY_BUFFER,waterIndexBufferID);
	glBufferData(GL_ELEMENT_ARRAY_BUFFER,sizeof(GLuint)*waterindex.size(),&(waterindex[0]),GL_DYNAMIC_DRAW);

	glBindBuffer(GL_ARRAY_BUFFER,waterCoordBufferID);
	glBufferData(GL_ARRAY_BUFFER,sizeof(GLfloat)*watercoords.size(),&(watercoords[0]),GL_DYNAMIC_DRAW);

	glBindVertexArray(waterVertArrayBuffer);

	glEnableVertexAttribArray(0);

	glBindBuffer(GL_ARRAY_BUFFER,waterCoordBufferID);
	glVertexAttribPointer(0,3,GL_FLOAT,GL_FALSE,0,0);

	glBindBuffer(GL_ELEMENT_ARRAY_BUFFER,waterIndexBufferID);
	
	glBindVertexArray(0);
}

void ti::WATER::render_water()
{
	glUniform1f(shader_->getUniformLocation("water_level"), waterlevel);
	glUniform1f(shader_->getUniformLocation("d_time"), CalculateTime::getElapsedTime());

	glActiveTexture(GL_TEXTURE0);
	glBindTexture(GL_TEXTURE_2D, waterReflection.fbo_texture);

	glActiveTexture(GL_TEXTURE1);
	glBindTexture(GL_TEXTURE_2D, waterRefraction.fbo_texture);

	glActiveTexture(GL_TEXTURE2);
	glBindTexture(GL_TEXTURE_2D, normal_map.textureid);

	glActiveTexture(GL_TEXTURE3);
	glActiveTexture(GL_TEXTURE4);
	glActiveTexture(GL_TEXTURE5);
	glBindTexture(GL_TEXTURE_2D, foam.textureid);

	glUniform1i(shader_->getUniformLocation("reflection_texture"), 0);
	glUniform1i(shader_->getUniformLocation("refraction_texture"), 1);
	glUniform1i(shader_->getUniformLocation("bump_map"), 2);
	glUniform1i(shader_->getUniformLocation("terrain_position"), 3);
	glUniform1i(shader_->getUniformLocation("terrain_normal"), 4);
	glUniform1i(shader_->getUniformLocation("foam"), 5);

    glBindVertexArray(waterVertArrayBuffer);
	for(unsigned long indRow=0;indRow<indexRows;indRow++)
	{
		glDrawElements(GL_TRIANGLES,indexRange,GL_UNSIGNED_INT,(void*)(sizeof(unsigned long)*indexRange*indRow));
	}
	glBindVertexArray(GL_NONE);
};

void ti::WATER::incWaterHeight()
{
	waterlevel+=0.05;
}
void ti::WATER::initialize()
{
	waterReflection.createFBO(1024, 1024);
	waterGlobalReflection.createFBO(1024, 1024);
	waterRefraction.createFBO(1024, 1024);

	init_water();
}
void ti::WATER::update()
{
}
void ti::WATER::renderInternal()
{
	render_water();
}

void ti::WATER::decrWaterHeight()
{
	waterlevel-=0.05;
};