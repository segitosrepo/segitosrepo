#ifndef WATER_H
#define WATER_H
#define MAX_FRAMES 32

#include <Windows.h>
#include <iostream>
#include <vector>
#include <math.h>

#include "GLIncludes.h"
#include "fbomanager.h"

#include "pndtexture.h"
#include "Shader.h"

#include "ObjectLoader.h"

namespace ti {
	class WATER : public Object
	{
	public:

		WATER(Shader& shader, int w, int h);

		int width;
		int height;

		std::vector<float> watercoords;
		std::vector<unsigned int> waterindex;
		std::vector<float> waterdepth;

		void loadWater();
		void init_water();
		void render_water();

		PNDTEXTURE normal_map;
		PNDTEXTURE foam;

		GLuint waterCoordBufferID;
		GLuint waterIndexBufferID;
		GLuint waterDepthBufferID;
		GLuint waterVertArrayBuffer;

		FBOMANAGER waterReflection;
		FBOMANAGER waterGlobalReflection;
		FBOMANAGER waterRefraction;

		float waterlevel;
		void decrWaterHeight();
		void incWaterHeight();

		unsigned int indexRange;
		unsigned int indexRows;

		void initialize();
		void update();
		void renderInternal();
	};

}
#endif