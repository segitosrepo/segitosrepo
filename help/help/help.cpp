// grafikaBeadando.cpp : Defines the entry point for the console application.
//
#define GLEW_STATIC

#include <GL\glew.h>
#include <GLFW\glfw3.h>

#include <iostream>
#include <string>
#include <chrono>

#include "Classes\Camera.h"
#include "Classes\Control.h"
#include "Classes\RenderScene.h"
#include "Classes\TimeCalculator.h"
#include "Classes\HeightMapLoader.h"
#include "Classes\Water.h"


static const float PI = 3.141592f;

struct WindowContextSettings {

	std::string title;
	int width;
	int height;
	int oglMajorVersion;
	int oglMinorVersion;
	bool wireframe;

	WindowContextSettings()
	{
		this->title = "Grafika Beadando";
		this->width = 1024;
		this->height = 768;
		this->oglMajorVersion = 4;
		this->oglMinorVersion = 5;
		this->wireframe = false;
	}
};

static WindowContextSettings windowSettings;

static void framebufferSize(GLFWwindow* window, int width, int height)
{
	windowSettings.width = width;
	windowSettings.height = height;
}

static void error(int error, const char* description)
{
	std::cerr << "Error code " << error << ": " << description << "\n";
};

int main(void)
{
	glfwSetErrorCallback(error);

	if (GLFW_TRUE != glfwInit())
	{
		return -1;
	}

	std::cout << "GLFW initialized\n";

	glfwWindowHint(GLFW_CONTEXT_VERSION_MAJOR, windowSettings.oglMajorVersion);
	glfwWindowHint(GLFW_CONTEXT_VERSION_MINOR, windowSettings.oglMinorVersion);

	GLFWwindow* window = glfwCreateWindow(windowSettings.width, windowSettings.height, windowSettings.title.c_str(), NULL, NULL);

	if (!window)
	{
		return -1;
	}

	glfwMakeContextCurrent(window);


	const GLenum err = glewInit();

	if (GLEW_OK != err)
	{
		std::cout << "GLEW Error: " << glewGetErrorString(err) << "\n";
		return -1;
	}

	std::cout << "GLEW initialized\n";

	std::cout << "OpenGL version: " << (char*)glGetString(GL_VERSION) << "\n";
	std::cout << "GLSL version: " << (char*)glGetString(GL_SHADING_LANGUAGE_VERSION) << "\n";
	std::cout << "OpenGL vendor: " << (char*)glGetString(GL_VENDOR) << "\n";

	glfwSetFramebufferSizeCallback(window, framebufferSize);
	glfwSetKeyCallback(window, ti::CONTROL::keyMapping);
	glfwSetCursorPosCallback(window, ti::CONTROL::cursorPosition);
	glfwSetMouseButtonCallback(window, ti::CONTROL::mouseButtonCallback);

	ti::CONTROL::cameraState.cameraSpeed = 50.0f;

	ti::SceneSettings sceneSettings;
	sceneSettings.shadowmapResolution = glm::ivec2(1024, 1024);
	ti::RenderScene scene("demo");

	scene.initialize(sceneSettings);


	ti::Camera camera;
	scene.addCamera(&camera);
	scene.setActiveCamera(0);
	
	ti::Shader terrainShader;
	terrainShader.load("Shaders/Terrain/terrainVertex.txt", "Shaders/Terrain/terrainFragment.txt", 0, 0);
	ti::Shader waterShader;
	waterShader.load("Shaders/Water/waterVertex.txt",
		"Shaders/Water/waterFragment.txt",
		0, 0);

	ti::HeightMap heightMap(terrainShader);
	ti::WATER water(waterShader, 1000, 1000);

	scene.addObject(&heightMap);
	scene.setWater(&water);


	camera.setPosition(glm::vec3(2, 50, 2));
	camera.lookAt(glm::vec3(0, 0, 0));
	camera.setParameters(60, 1, 1000);

	glEnable(GL_DEPTH_TEST);
	glDepthFunc(GL_LESS);
	glEnable(GL_CULL_FACE);

	glClearColor(0.5f, 0.5f, 0.5f, 1.0f);
	glfwSwapInterval(0);

	ti::CalculateTime::init();

	while (!glfwWindowShouldClose(window))
	{
		ti::CalculateTime::calculateFrameTime();

		if (windowSettings.wireframe)
		{
			glPolygonMode(GL_FRONT_AND_BACK, GL_LINE);
			glDisable(GL_CULL_FACE);
		}
		else
		{
			glPolygonMode(GL_FRONT_AND_BACK, GL_FILL);
			glEnable(GL_CULL_FACE);
		}


		ti::CONTROL::cursorPositionUpdate();

		camera.update(windowSettings.width, windowSettings.height);

		ti::CONTROL::cameraMovement(scene.getActiveCamera(), ti::CONTROL::cameraState.movementState, (float)ti::CalculateTime::getFrameDelta(), ti::CONTROL::cameraState.cameraSpeed);
		ti::CONTROL::cameraRotation(scene.getActiveCamera());

		scene.update();
		scene.render();

		glfwSwapBuffers(window);
		glfwPollEvents();
	}
	return 0;
}