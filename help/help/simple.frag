#version 400

in vec3 vpos;

out vec3 color;

void main()
{
	color = vec3(fract(vpos*0.99f));
}