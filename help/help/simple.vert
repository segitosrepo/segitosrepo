#version 400

layout(location=0) in vec3 vertex_position;

uniform mat4 model_matrix;
uniform mat4 view_matrix;
uniform mat4 projection_matrix;

out vec3 vpos;

void main()
{
	vpos = vertex_position;
	gl_Position = projection_matrix*view_matrix*model_matrix*vec4(vertex_position, 1);
}