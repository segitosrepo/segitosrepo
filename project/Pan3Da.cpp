﻿/*PAN3DA PROJECT 1.0*/

#include "pnd.h"

int main(int argc, char** argv)
{
	const int windowWidth = 1280;
	const int windowHeight = 1024;
	const int windowFullscreen = false;

	PND programWindow;
	programWindow.attachRend(&renderer);
	programWindow.attachCam(&camera);
	programWindow.attachCont(&control);

	if (!programWindow.create(windowWidth, windowHeight, windowFullscreen))
	{
		programWindow.destroy();
		return -1;
	}
	if (!renderer.init())
	{
		return -1;
	}

	renderer.load();

	while(programWindow.isRunning())
	{
		programWindow.processEvents();
		renderer.render();
		programWindow.swapBuffers();
	}
	renderer.shutdown();
	programWindow.destroy();
	return 0;
}