#include "dsh.h"

GBUFFER::GBUFFER(){};
GBUFFER::~GBUFFER(){};

void GBUFFER::InitRenderTexture(GLuint& texture, GLenum attachment){
   glGenTextures(1, &texture);
   glBindTexture(GL_TEXTURE_2D, texture);

   glTexParameteri(GL_TEXTURE_2D, GL_TEXTURE_MIN_FILTER, GL_NEAREST);
   glTexParameteri(GL_TEXTURE_2D, GL_TEXTURE_MAG_FILTER, GL_NEAREST);
   glTexParameteri(GL_TEXTURE_2D, GL_TEXTURE_WRAP_S, GL_CLAMP_TO_EDGE);
   glTexParameteri(GL_TEXTURE_2D, GL_TEXTURE_WRAP_T, GL_CLAMP_TO_EDGE);
   glTexImage2D(GL_TEXTURE_2D, 0, GL_RGBA32F, textureSize[0], textureSize[1], 0, GL_RGBA, GL_FLOAT, NULL);
   glBindTexture(GL_TEXTURE_2D, 0);

   glFramebufferTexture2D(GL_FRAMEBUFFER, attachment, GL_TEXTURE_2D, texture, 0);
}

void GBUFFER::InitDepthTexture(){
   glGenTextures(1, &tDepthTexture);
   glBindTexture(GL_TEXTURE_2D, tDepthTexture);
   glTexImage2D(GL_TEXTURE_2D, 0, GL_DEPTH_COMPONENT32F, textureSize[0], textureSize[1], 0, GL_DEPTH_COMPONENT, GL_FLOAT, NULL);
   glTexParameteri(GL_TEXTURE_2D, GL_TEXTURE_COMPARE_MODE, GL_NONE);
   glTexParameteri( GL_TEXTURE_2D, GL_TEXTURE_MIN_FILTER, GL_NEAREST);
   glTexParameteri( GL_TEXTURE_2D, GL_TEXTURE_MAG_FILTER, GL_NEAREST);
   glTexParameteri( GL_TEXTURE_2D, GL_TEXTURE_WRAP_S, GL_CLAMP_TO_EDGE );
   glTexParameteri( GL_TEXTURE_2D, GL_TEXTURE_WRAP_T, GL_CLAMP_TO_EDGE );
   glBindTexture(GL_TEXTURE_2D, 0);
   glFramebufferTexture2D(GL_FRAMEBUFFER, GL_DEPTH_ATTACHMENT, GL_TEXTURE_2D, tDepthTexture, 0);
}

bool GBUFFER::Init(unsigned int WindowWidth, unsigned int WindowHeight)
{
	textureSize[0]=WindowWidth;
	textureSize[1]=WindowHeight;

    glGenFramebuffers(1, &gFBO);
    glBindFramebuffer(GL_FRAMEBUFFER, gFBO);

	InitRenderTexture(tTextures[DIFFUSE],GL_COLOR_ATTACHMENT0);
	InitRenderTexture(tTextures[POSITION],GL_COLOR_ATTACHMENT1);
	InitRenderTexture(tTextures[NORMAL],GL_COLOR_ATTACHMENT2);
	InitDepthTexture();

	glGenRenderbuffers(1, &gRBO);
	glBindRenderbuffer(GL_RENDERBUFFER, gRBO);
	glRenderbufferStorage(GL_RENDERBUFFER, GL_DEPTH_COMPONENT32F, WindowWidth, WindowHeight);
	glFramebufferRenderbuffer(GL_FRAMEBUFFER, GL_DEPTH_ATTACHMENT, GL_RENDERBUFFER, gRBO);

	GLenum attachmentList[] = {GL_COLOR_ATTACHMENT0, GL_COLOR_ATTACHMENT1,GL_COLOR_ATTACHMENT2};
    glDrawBuffers(3, attachmentList);

	GLenum status = glCheckFramebufferStatus(GL_FRAMEBUFFER);
    if(status!= GL_FRAMEBUFFER_COMPLETE) {return false;}

	glBindFramebuffer(GL_FRAMEBUFFER, 0);

	return true;
}

void GBUFFER::Bind()
{
	glBindFramebuffer(GL_FRAMEBUFFER,0);
	glBindFramebuffer(GL_FRAMEBUFFER,gFBO);
	glDisable(GL_SCISSOR_TEST);
	glClear(GL_COLOR_BUFFER_BIT | GL_DEPTH_BUFFER_BIT);
	glEnable(GL_SCISSOR_TEST);
}

void GBUFFER::BindForWriting()
{
	glBindFramebuffer(GL_FRAMEBUFFER,gFBO);
	glPushAttrib(GL_VIEWPORT_BIT);
	glViewport(0,0,textureSize[0], textureSize[1]);
    glBindFramebuffer(GL_DRAW_FRAMEBUFFER, gFBO);
}

void GBUFFER::BindForReading()
{
	glBindFramebuffer(GL_FRAMEBUFFER,gFBO);
	glPushAttrib(GL_VIEWPORT_BIT);
	glViewport(0,0,textureSize[0], textureSize[1]);
    glBindFramebuffer(GL_READ_FRAMEBUFFER, gFBO);
}

void GBUFFER::SetReadBuffer(GBUFFER_TEXTURE_TYPE TextureType)
{
    glReadBuffer(GL_COLOR_ATTACHMENT0 + TextureType);
}

void GBUFFER::UnBind()
{

	glBindFramebuffer(GL_FRAMEBUFFER,0);
}

GLuint* GBUFFER::GetTextureSize()
{
	return textureSize;
}

void GBUFFER::ResizeTexture(GLuint width,GLuint height)
{
	if(gFBO!=0){
	textureSize[0]=width;
	textureSize[1]=height;
	for(int i = 0;i<GBUFFER_NUM_TEXTURES;i++){
		glBindTexture(GL_TEXTURE_2D, tTextures[DIFFUSE]);
		glTexImage2D(GL_TEXTURE_2D, 0, GL_RGBA, textureSize[0], textureSize[1], 0, GL_RGBA, GL_UNSIGNED_BYTE, NULL);
	}
	glBindTexture(GL_TEXTURE_2D, 0);

	glBindRenderbuffer(GL_RENDERBUFFER, gRBO);
    glRenderbufferStorage(GL_RENDERBUFFER, GL_DEPTH_COMPONENT, width, height);
	glBindRenderbuffer(GL_RENDERBUFFER, 0);
	}
};

GBUFFER gbuffer;