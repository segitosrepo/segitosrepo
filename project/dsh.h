#include <Windows.h>
#include <stdio.h>

#include <GL/glew.h>

#ifndef PND_G_BUFFER_H
#define PND_G_BUFFER_H

#define ARRAY_SIZE_IN_ELEMENTS(A)    ((sizeof((A)))/(sizeof(unsigned int)))

class GBUFFER
{
public:

    enum GBUFFER_TEXTURE_TYPE {
        POSITION,
		NORMAL,
        DIFFUSE,
        GBUFFER_NUM_TEXTURES
    };
    GBUFFER();
    ~GBUFFER();
    bool Init(unsigned int, unsigned int);
	void Bind();
    void BindForWriting();
    void BindForReading();
	void SetReadBuffer(GBUFFER_TEXTURE_TYPE);
	void UnBind();

	void InitRenderTexture(GLuint&,GLenum);
	void InitDepthTexture();

	void ResizeTexture(GLuint,GLuint);

	GLuint* GetTextureSize();

	GLuint tDepthTexture;
	GLuint tTextures[GBUFFER_NUM_TEXTURES];

private:
    GLuint gFBO;
	GLuint gRBO;
	GLuint textureSize[2];

};
#endif
extern GBUFFER gbuffer;