#include <stdio.h>

#include <GL/glew.h>

#ifndef PND_FBO_H
#define PND_FBO_H

class FBOMANAGER
{
public:

	bool createFBO(GLuint,GLuint);
	bool createDepthFBO(GLuint,GLuint);
	bool createShadowDepthFBO(GLuint,GLuint);
	void reshapeFBO(unsigned int width,unsigned int height);
	void bindFBO();
	static void unbindFBO();
	GLuint fbo_texture;
private:
	GLuint FBO;
	GLuint RBO;
	GLuint width;
	GLuint height;
};

#endif