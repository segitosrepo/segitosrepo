﻿#include "pnd.h"

PND::PND():m_isRunning(false),m_renderer(NULL),m_lastTime(0)
{};

bool PND::create(int width, int height, bool fullscreen)
{
	getAttachedCont()->init(*getAttachedRend(), *getAttachedCam());
	m_isFullscreen = fullscreen;

	if (!glfwInit()) return false;

	window = glfwCreateWindow(640, 480, "Hello World", NULL, NULL);
    if (!window)
    {
        glfwTerminate();
        return false;
    }

    glfwMakeContextCurrent(window);
	return true;
};

void PND::processEvents()
{
    glfwPollEvents();
};

void PND::destroy() 
{
    glfwTerminate();
}

void PND::swapBuffers()
{
	glfwSwapBuffers(window);
}

bool PND::isRunning()
{
	return (m_isRunning);
};

void PND::attachRend(PNDRENDER* render)
{
	m_renderer = render;
};

void PND::attachCam(PNDCAMERA* camera)
{
	m_camera=camera;
};

void PND::attachCont(PNDCONTROL* control)
{
	m_control=control;
};
