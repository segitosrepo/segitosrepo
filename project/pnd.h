#include "pndrender.h"
#include "pndcamera.h"
#include "pndcontrol.h"

#include <GL/glew.h>
#include <GLFW/glfw3.h>

#include <ctime>

class PND
{
public:
	PND();

	 GLFWwindow* window;

	int height;
	int width;

	PNDCAMERA* m_camera;
	PNDRENDER* m_renderer;
	PNDCONTROL* m_control;

	bool m_isRunning;
	bool m_isFullscreen;
	float m_lastTime;

	bool create(int, int, bool);
	void processEvents();
	void destroy();
	void swapBuffers();
	bool isRunning();

	void attachRend(PNDRENDER*);
	PNDRENDER* getAttachedRend(){return m_renderer;};

	void attachCam(PNDCAMERA*);
	PNDCAMERA* getAttachedCam(){return m_camera;};

	void attachCont(PNDCONTROL*);
	PNDCONTROL* getAttachedCont(){return m_control;};
};