#include "GLSLProgram.h"

#include "pndcamera.h"

#include <ctime>
#include <math.h>

#define C_SPEED 0.05

PNDCAMERA::PNDCAMERA()
{ 
	model_matrix=glm::mat4x4(1.0f);
	view_matrix=glm::mat4x4(1.0f);
	projection_matrix=glm::mat4x4(1.0f);

	cameraPosition=glm::vec3(0);

	horizontalAngle=0.0f;
	verticalAngle=0.0f;

	moveForw=false;
	moveBack=false;
	moveRight=false;
	moveLeft=false;
	hAngle=false;
	vAngle=false;

	cmc.d_horangle_prev=0;
	cmc.d_verangle_prev=0;
	cmc.horizontalAngle=0;
	cmc.verticalAngle=0;
	cmc.p_cursorPos.x=cmc.p_cursorPos.y=0;
	cmc.p_origin.x=cmc.p_origin.y=0;

	screenWidth=uiWindowRect.right;
	screenHeight=uiWindowRect.bottom;
};

PNDCAMERA::PNDCAMERA(float posx,float posy,float posz,float hAngle,float vAngle)
{
	cameraPosition=glm::vec3(posx,posy,posz);
	horizontalAngle=hAngle;
	verticalAngle=vAngle;
};

void PNDCAMERA::renderMatrix(int cposx, int cposy, int windowHeight, int windowWidth)
{
	float dttime=getElapsedSeconds();



	double d_horangle=((double)cmc.p_cursorPosX-(double)cmc.p_originX)/(double)screenWidth*PI;
	double d_verangle=((double)cmc.p_cursorPosY-(double)cmc.p_originY)/(double)screenHeight*PI;

	cmc.horizontalAngle=d_horangle+cmc.d_horangle_prev;
    cmc.verticalAngle=d_verangle+cmc.d_verangle_prev;

	if(cmc.verticalAngle>PI/2) cmc.verticalAngle=PI/2;
	if(cmc.verticalAngle<-PI/2) cmc.verticalAngle=-PI/2;

	changevAngle(cmc.verticalAngle);
    changehAngle(cmc.horizontalAngle);

	rightVector=glm::vec3(sin(horizontalAngle - PI/2.0f),0,cos(horizontalAngle - PI/2.0f));
	directionVector=glm::vec3(cos(verticalAngle) * sin(horizontalAngle), sin(verticalAngle), cos(verticalAngle) * cos(horizontalAngle));

	upVector=glm::vec3(glm::cross(rightVector,directionVector));

    glm::normalize(upVector);
	glm::normalize(directionVector);
	glm::normalize(rightVector);


	if(moveForw==true)
	{
		cameraPosition=cameraPosition+directionVector*(float)C_SPEED*dttime;
	}
	if(moveBack==true)
	{
		cameraPosition=cameraPosition-directionVector*(float)C_SPEED*dttime;
	}
	if(moveRight==true)
	{
		cameraPosition=cameraPosition+rightVector*(float)C_SPEED*dttime;
	}
	if(moveLeft==true)
	{
		cameraPosition=cameraPosition-rightVector*(float)C_SPEED*dttime;
	}

	glViewport(0,0,screenWidth,screenHeight);
	glScissor(0,0,screenWidth,screenHeight);

	projection_matrix=glm::perspective(60.0f, float(screenWidth) / float(screenHeight), 1.0f, 1000.0f);

	view_matrix = glm::lookAt(
		cameraPosition,
		cameraPosition+directionVector,
		upVector);

	terrainShader->bindShader();
	terrainShader->sendUniform4x4("model_matrix",glm::value_ptr(model_matrix));
	terrainShader->sendUniform4x4("view_matrix",glm::value_ptr(view_matrix));
	terrainShader->sendUniform4x4("projection_matrix",glm::value_ptr(projection_matrix));
	waterShader->bindShader();
    waterShader->sendUniform4x4("model_matrix",glm::value_ptr(model_matrix));
	waterShader->sendUniform4x4("view_matrix",glm::value_ptr(view_matrix));
	waterShader->sendUniform4x4("projection_matrix",glm::value_ptr(projection_matrix));
	skyShader->bindShader();
	skyShader->sendUniform4x4("model_matrix",glm::value_ptr(model_matrix));
	skyShader->sendUniform4x4("view_matrix",glm::value_ptr(view_matrix));
	skyShader->sendUniform4x4("projection_matrix",glm::value_ptr(projection_matrix));
};

void PNDCAMERA::changePosForward(bool keydown)
{
	if(keydown==true)
	moveForw=true;
	else moveForw=false;
};

void PNDCAMERA::changePosBackward(bool keydown)
{
	if(keydown==true)
	moveBack=true;
	else moveBack=false;
};

void PNDCAMERA::changePosRight(bool keydown)
{
	if(keydown==true)
	moveRight=true;
	else moveRight=false;
};

void PNDCAMERA::changePosLeft(bool keydown)
{
	if(keydown==true)
	moveLeft=true;
	else moveLeft=false;
};

void PNDCAMERA::changehAngle(float hangle)
{
	if(hAngle==true)
	horizontalAngle=hangle;
};

void PNDCAMERA::changevAngle(float vangle)
{
	if(vAngle==true)
	verticalAngle=vangle;
};

void PNDCAMERA::ifhAngle(bool bdown)
{
	if(bdown==true)
	hAngle=true;
	else hAngle=false;
};

void PNDCAMERA::ifvAngle(bool bdown)
{
	if(bdown==true)
	vAngle=true;
	else vAngle=false;
};

void PNDCAMERA::strt_rotateCamera()
{
	cmc.p_origin=cmc.p_cursorPos;
	ifvAngle(true);
	ifhAngle(true);
};

void PNDCAMERA::nd_rotateCamera()
{
	cmc.d_horangle_prev=horizontalAngle;
	cmc.d_verangle_prev=verticalAngle;
	ifvAngle(false);
	ifhAngle(false);
};

PNDCAMERA camera;