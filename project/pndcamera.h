#include <GL/glew.h>

#include "pndsystem.h"

#include <GLM\glm.hpp>
#include <GLM\gtc\matrix_transform.hpp>
#include <GLM\gtc\type_ptr.hpp>

#define PI 3.141592

#define _CRT_TERMINATE_DEFINED

#ifndef PNDCAMERA_H
#define PNDCAMERA_H

struct CMC
{
	int p_cursorPosX;
	int p_cursorPosY;
	int p_originX;
	int p_originY;

	double horizontalAngle;
	double verticalAngle;
	double d_horangle_prev;
	double d_verangle_prev;
};

class PNDCAMERA:public PNDSYSTEM
{
public:

	glm::vec3 cameraPosition;
	double horizontalAngle;
	double verticalAngle;

	int screenWidth;
	int screenHeight;
	glm::vec3 directionVector;
    glm::vec3 rightVector;
	glm::vec3 upVector;
	
	void renderMatrix(int cposx, int cposy, int windowHeight, int windowWidth);

	void changePosForward(bool);
	void changePosBackward(bool);
	void changePosRight(bool);
	void changePosLeft(bool);
	void ifvAngle(bool bdown);
	void ifhAngle(bool bdown);
	void changehAngle(float);
	void changevAngle(float);

	void strt_rotateCamera();
	void nd_rotateCamera();

	PNDCAMERA();
	PNDCAMERA(GLfloat,GLfloat,GLfloat,GLfloat,GLfloat);

	glm::mat4x4 model_matrix;
	glm::mat4x4 projection_matrix;
	glm::mat4x4 view_matrix;

	bool moveForw;
	bool moveBack;
	bool moveRight;
	bool moveLeft;
	bool hAngle;
	bool vAngle;
	CMC cmc;
};
#endif
extern PNDCAMERA camera;