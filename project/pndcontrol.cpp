#include "pndcontrol.h"
#include "pndmaprender.h"

void PNDCONTROL::controlSystem(WPARAM& wParam,LPARAM& lParam,UINT& uMsg)
{
	switch(uMsg){
	case WM_SIZE:
		getAttachedCam()->screenHeight = HIWORD(lParam);
		getAttachedCam()->screenWidth = LOWORD(lParam);
		break;
	case WM_KEYDOWN:
		if (wParam == VK_UP||wParam == 0x57)
		{
			getAttachedCam()->changePosForward(true);
		}
		else if (wParam == VK_DOWN||wParam == 0x53)
		{
			getAttachedCam()->changePosBackward(true);
		}
		else if (wParam == VK_RIGHT||wParam==0x44)
		{
			getAttachedCam()->changePosRight(true);
		}
		else if (wParam == VK_LEFT||wParam==0x41)
		{
			getAttachedCam()->changePosLeft(true);
		}
		else if (wParam == VK_F1)
		{
			getAttachedRend()->rendermode();
		}
		else if (wParam == VK_ADD)
		{
			ocean.incWaterHeight();
		}
		else if (wParam == VK_SUBTRACT)
		{
			ocean.decrWaterHeight();
		}
		else if (wParam == VK_NUMPAD4)
		{
			sky.controlLight(3);
		}
		else if (wParam == VK_NUMPAD5)
		{
			sky.controlLight(5);
		}
		else if (wParam == VK_NUMPAD6)
		{
			sky.controlLight(4);
		}
		else if (wParam == VK_NUMPAD8)
		{
			sky.controlLight(1);
		}
		else if (wParam == VK_NUMPAD2)
		{
			sky.controlLight(2);
		}
		break;
	case WM_KEYUP:
		if (wParam == VK_UP||wParam == 0x57)
		{
		getAttachedCam()->changePosForward(false);
		}
		if (wParam == VK_DOWN||wParam == 0x53)
		{
			getAttachedCam()->changePosBackward(false);
		}
		if (wParam == VK_RIGHT||wParam==0x44)
		{
			getAttachedCam()->changePosRight(false);
		}
		if (wParam == VK_LEFT||wParam==0x41)
		{
			getAttachedCam()->changePosLeft(false);
		}
		break;
	case WM_LBUTTONDOWN:
		getAttachedCam()->strt_rotateCamera();
		break;
	case WM_LBUTTONUP:
		getAttachedCam()->nd_rotateCamera();
		break;
}
};

void PNDCONTROL::attachRend(PNDRENDER* render)
{
	c_renderer = render;
};

void PNDCONTROL::attachCam(PNDCAMERA* camera)
{
	c_camera = camera;
};

void PNDCONTROL::gameControl()
{

}

bool PNDCONTROL::init(PNDRENDER& renderer,PNDCAMERA& camera)
{
	attachRend(&renderer);
	attachCam(&camera);
	return true;
};

PNDCONTROL control;