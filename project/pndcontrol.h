#include "pndrender.h"
#include "pndcamera.h"

#ifndef PNDCONTROL_H
#define PNDCONTROL_H

class PNDCONTROL{
public:
	bool init(PNDRENDER&,PNDCAMERA&);
	void controlSystem(WPARAM&,LPARAM&,UINT&);
	void displayinterface();

	void attachRend(PNDRENDER*);
	void attachCam(PNDCAMERA*);

	PNDCAMERA* getAttachedCam(){return c_camera;};
	PNDRENDER* getAttachedRend(){return c_renderer;};
private:
	PNDRENDER* c_renderer;
	PNDCAMERA* c_camera;
	void gameControl();
};
#endif
extern PNDCONTROL control;