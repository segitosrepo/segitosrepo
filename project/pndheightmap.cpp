#include "pndheightmap.h"
#include "dsh.h"

void PNDHEIGHTMAP::loadHeightmap(const char* filename,float heightscale)
{
	heightScale=heightscale;
	memset(&fileheader,0,(sizeof(BITMAPFILEHEADER)));
	memset(&infoheader,0,(sizeof(BITMAPINFOHEADER)));

	fopen_s(&fp,filename, "rb");
	fread (&fileheader, sizeof(BITMAPFILEHEADER), 1, fp);
	fread (&infoheader,sizeof(BITMAPINFOHEADER), 1, fp);

	image_size = infoheader.biWidth * infoheader.biHeight * 3;
	data = new unsigned char[image_size+2];

	fseek(fp, fileheader.bfOffBits, SEEK_SET);
	fread (data,1,image_size+1, fp);

	mapwidth=infoheader.biWidth;
	mapheight=infoheader.biHeight;

	int k=0;
	int ind = 0;

	coords.reserve(mapheight*mapwidth*3);
	mapindex.reserve((mapheight-1)*(mapwidth-1)*6);
	for(int row=0;row<mapheight;row++)
	{
		for(int col=0;col<mapwidth;col++)
		{
			height=data[k];

	    	coords.push_back((float)col);
			coords.push_back((float)(height/heightscale));
			coords.push_back((float)row);

			k+=3;
		}
	}

	for(int row=0;row<mapheight-1;row++)
	{
		for(int col=0;col<mapwidth-1;col++)
		{
			mapindex.push_back(row*mapwidth+col);
			mapindex.push_back((row+1)*mapwidth+col);
			mapindex.push_back(row*mapwidth+col+1);

			mapindex.push_back(row*mapwidth+col+1);
            mapindex.push_back((row+1)*mapwidth+col);
			mapindex.push_back((row+1)*mapwidth+col+1);
		}
	}

	indexRange=6*mapwidth;
	indexRows=mapheight-1;
}

void PNDHEIGHTMAP::initHeightmap(const char*filename,float heightscale){

	terrainShader->bindShader();
	loadHeightmap(filename,heightscale);
	getVertexNormal();

	terr_shore = PNDTEXTURE::load_texture("sand.BMP");
	terr_texture1 = PNDTEXTURE::load_texture("grass.BMP");
	terr_texture2 = PNDTEXTURE::load_texture("slope.BMP");
	terr_texture3 = PNDTEXTURE::load_texture("stone.BMP");

	glGenBuffers(1,&mapCoordBufferID);
	glGenBuffers(1,&mapIndexBufferID);
	glGenBuffers(1,&mapNormalBufferID);

	glGenVertexArrays(1,&terrainVertArrayBuffer);

	glBindBuffer(GL_ELEMENT_ARRAY_BUFFER,mapIndexBufferID);
	glBufferData(GL_ELEMENT_ARRAY_BUFFER,sizeof(GLuint)*mapindex.size(),&mapindex[0],GL_STATIC_DRAW);

	glBindBuffer(GL_ARRAY_BUFFER,mapCoordBufferID);
	glBufferData(GL_ARRAY_BUFFER,sizeof(GLfloat)*coords.size(),&coords[0],GL_STATIC_DRAW);

	glBindBuffer(GL_ARRAY_BUFFER,mapNormalBufferID);
	glBufferData(GL_ARRAY_BUFFER,sizeof(GLfloat)*mapnormal.size(),&mapnormal[0],GL_STATIC_DRAW);

	glBindVertexArray(terrainVertArrayBuffer);

	glEnableVertexAttribArray(0);
	glEnableVertexAttribArray(1);

	glBindBuffer(GL_ARRAY_BUFFER,mapCoordBufferID);
	glVertexAttribPointer(0,3,GL_FLOAT,GL_FALSE,0,0);

	glBindBuffer(GL_ARRAY_BUFFER,mapNormalBufferID);
    glVertexAttribPointer(1,3,GL_FLOAT,GL_FALSE,0,0);

	glBindBuffer(GL_ELEMENT_ARRAY_BUFFER,mapIndexBufferID);

	glBindVertexArray(0);
}

void PNDHEIGHTMAP::renderHeightmap()
{
	glBindVertexArray(terrainVertArrayBuffer);
	for(unsigned long indRow=0;indRow<indexRows;indRow++)
	{glDrawElements(GL_TRIANGLES,indexRange,GL_UNSIGNED_INT,(void*)(sizeof(unsigned int)*indexRange*indRow));}
	glBindVertexArray(GL_NONE);
}

glm::vec3 PNDHEIGHTMAP::getCoordData(int c_x, int c_z)
{
	glm::vec3 coord;
	coord.x=c_x;
	coord.z=c_z;
	coord.y=(float)data[3*(c_x+(int)mapwidth*c_z)]/heightScale;
	return coord;
};

void PNDHEIGHTMAP::getVertexNormal()
{
	glm::vec3 vec[9];
	glm::vec3 vNormalCoords[8];
	glm::vec3 FinalNormal(0.0f);

	for(int z=0; z<mapwidth;z++)
	{
		mapnormal.push_back(0);
	    mapnormal.push_back(1);
		mapnormal.push_back(0);
	}

	for(int z=1; z<mapheight-1;z++)
	{
		mapnormal.push_back(0);
	    mapnormal.push_back(1);
	    mapnormal.push_back(0);

		for(int x=1; x<mapwidth-1;x++)
		{
			vec[0]=getCoordData(x,z);

			vec[1]=getCoordData(x+1,z);
			vec[2]=getCoordData(x-1,z);
			vec[3]=getCoordData(x,z-1);
			vec[4]=getCoordData(x,z+1);
			vec[5]=getCoordData(x+1,z-1);
			vec[6]=getCoordData(x-1,z+1);
			vec[7]=getCoordData(x+1,z+1);
			vec[8]=getCoordData(x-1,z-1);

			vNormalCoords[0]=glm::cross(glm::normalize(vec[1]-vec[0]),glm::normalize(vec[5]-vec[0]));
			vNormalCoords[1]=glm::cross(glm::normalize(vec[4]-vec[0]),glm::normalize(vec[5]-vec[0]));
			vNormalCoords[2]=glm::cross(glm::normalize(vec[8]-vec[0]),glm::normalize(vec[4]-vec[0]));
			vNormalCoords[3]=glm::cross(glm::normalize(vec[8]-vec[0]),glm::normalize(vec[2]-vec[0]));
			vNormalCoords[4]=glm::cross(glm::normalize(vec[2]-vec[0]),glm::normalize(vec[6]-vec[0]));
			vNormalCoords[5]=glm::cross(glm::normalize(vec[3]-vec[0]),glm::normalize(vec[6]-vec[0]));
			vNormalCoords[6]=glm::cross(glm::normalize(vec[7]-vec[0]),glm::normalize(vec[3]-vec[0]));
			vNormalCoords[7]=glm::cross(glm::normalize(vec[7]-vec[0]),glm::normalize(vec[1]-vec[0]));

			FinalNormal =vNormalCoords[0];
			FinalNormal+=vNormalCoords[1];
			FinalNormal+=vNormalCoords[2];
			FinalNormal+=vNormalCoords[3];
			FinalNormal+=vNormalCoords[4];
			FinalNormal+=vNormalCoords[5];
			FinalNormal+=vNormalCoords[6];
			FinalNormal+=vNormalCoords[7];

			FinalNormal=glm::normalize(FinalNormal);

			mapnormal.push_back(FinalNormal.x);
			mapnormal.push_back(FinalNormal.y);
			mapnormal.push_back(FinalNormal.z);
		}
		
		mapnormal.push_back(0);
	    mapnormal.push_back(1);
	    mapnormal.push_back(0);
	}

	for(int z=0; z<mapwidth;z++)
	{
		mapnormal.push_back(0);
	    mapnormal.push_back(1);
		mapnormal.push_back(0);
	}
}