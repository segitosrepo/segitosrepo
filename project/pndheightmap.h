#include <Windows.h>
#include <iostream>
#include <vector>

#include <GL/glew.h>
#include <GLM\glm.hpp>
#include <GLM\gtc\matrix_transform.hpp>

#include "pndtexture.h"

#include "GLSLProgram.h"

using namespace std;

#ifndef PNDHEIGHTMAP_H
#define PNDHEIGHTMAP_H

class PNDHEIGHTMAP
{
public:
	GLuint
		terrainVertArrayBuffer,
		mapCoordBufferID,
		mapIndexBufferID,
		mapNormalBufferID;

	void loadHeightmap(const char*,float);

	void initHeightmap(const char*,float);
	void renderHeightmap();

	float heightScale;
	void getVertexNormal();

	std::vector<float> coords;
	std::vector<unsigned int> mapindex;
	std::vector<float> mapnormal;

	unsigned int indexRange;
	unsigned int indexRows;

	GLuint terr_texture1;
	GLuint terr_texture2;
	GLuint terr_texture3;
	GLuint terr_shore;

	float mapwidth;
	float mapheight;

	BITMAPFILEHEADER fileheader;
	BITMAPINFOHEADER infoheader;
	FILE *fp;
	unsigned char height;
    long image_size;
	unsigned char* data;

	glm::vec3 getCoordData(int, int);

};
#endif