#include "dsh.h"
#include "pndmaprender.h"
#include "pndcamera.h"

float PNDMAPRENDER::rendertime = 0;

PNDMAPRENDER::PNDMAPRENDER()
{

};
PNDMAPRENDER::~PNDMAPRENDER()
{

};

PNDMAPRENDER* maprenderer;

PNDWATER ocean;
PNDSKY sky;
PNDHEIGHTMAP heightmap;

void PNDMAPRENDER::loadOcean()
{
	ocean.init_water(heightmap.mapwidth, heightmap.mapheight);

	ocean.waterReflection.createFBO(camera.screenWidth, camera.screenHeight);
	ocean.waterGlobalReflection.createFBO(camera.screenWidth, camera.screenHeight);
	ocean.waterRefraction.createFBO(camera.screenWidth, camera.screenHeight);
};

void PNDMAPRENDER::prepareAttribs()
{
	rendertime = (float)glutGet(GLUT_ELAPSED_TIME);
	glClearColor(0, 0, 0, 0);

	ocean.waterGlobalReflection.bindFBO();
	skyShader->bindShader();
	skyShader->sendUniform("reflectionRender", 1);
	glFrontFace(GL_CW);
	sky.renderSkyBox();
	glFrontFace(GL_CCW);
	skyShader->sendUniform("reflectionRender", 0);

	//reflections
	ocean.waterReflection.bindFBO();
	terrainShader->bindShader();
	terrainShader->sendUniform("reflectionRender", 1);
	terrainShader->sendUniform("refractionRender", 0);
	glFrontFace(GL_CW);
	renderMap();
	glFrontFace(GL_CCW);
	terrainShader->sendUniform("reflectionRender", 0);

	//refraction
	ocean.waterRefraction.bindFBO();
	terrainShader->bindShader();
	terrainShader->sendUniform("reflectionRender", 0);
	terrainShader->sendUniform("refractionRender", 1);
	renderMap();
	terrainShader->sendUniform("refractionRender", 0);

	FBOMANAGER::unbindFBO();

	terrainShader->bindShader();
	terrainShader->sendUniform("water_level", ocean.waterlevel);
	terrainShader->sendUniform("global_light_direction", sky.global_light_direction.x, sky.global_light_direction.y, sky.global_light_direction.z);
	terrainShader->sendUniform("terr_texture1", 0);
	terrainShader->sendUniform("terr_texture2", 1);
	terrainShader->sendUniform("terr_texture3", 2);
	terrainShader->sendUniform("terr_tex_shore", 3);

	waterShader->bindShader();
	waterShader->sendUniform("global_light_direction", sky.global_light_direction.x, sky.global_light_direction.y, sky.global_light_direction.z);
	waterShader->sendUniform("camera_position", camera.cameraPosition.x, camera.cameraPosition.y, camera.cameraPosition.z);
	waterShader->sendUniform("d_time", rendertime);
	waterShader->sendUniform("water_level", ocean.waterlevel);
	waterShader->sendUniform("reflection_texture", 0);
	waterShader->sendUniform("refraction_texture", 1);
	waterShader->sendUniform("global_reflection_texture", 2);
	waterShader->sendUniform("terrain_position", 3);
	waterShader->sendUniform("bump_map", 4);
	waterShader->sendUniform("caustics", 5);
	waterShader->sendUniform("foam", 6);
	waterShader->sendUniform("terrain_normal", 7);
};

void PNDMAPRENDER::renderOcean()
{
	//water grid
	glEnable(GL_BLEND);
	glBlendFunc(GL_SRC_ALPHA, GL_ONE_MINUS_SRC_ALPHA);
	glPolygonOffset(0.01f, 0.01f);
	glEnable(GL_POLYGON_OFFSET_EXT);
	glDisable(GL_CULL_FACE);

	waterShader->bindShader();
	glActiveTexture(GL_TEXTURE0);
	glBindTexture(GL_TEXTURE_2D, ocean.waterReflection.fbo_texture);
	glActiveTexture(GL_TEXTURE1);
	glBindTexture(GL_TEXTURE_2D, ocean.waterRefraction.fbo_texture);
	glActiveTexture(GL_TEXTURE2);
	glBindTexture(GL_TEXTURE_2D, ocean.waterGlobalReflection.fbo_texture);
	glActiveTexture(GL_TEXTURE3);
	glBindTexture(GL_TEXTURE_2D, gbuffer.tTextures[gbuffer.POSITION]);
	glActiveTexture(GL_TEXTURE4);
	glBindTexture(GL_TEXTURE_2D, ocean.normal_map);
	glActiveTexture(GL_TEXTURE5);
	glBindTexture(GL_TEXTURE_2D, ocean.caustics_map);
	glActiveTexture(GL_TEXTURE6);
	glBindTexture(GL_TEXTURE_2D, ocean.foam);
	glActiveTexture(GL_TEXTURE7);
	glBindTexture(GL_TEXTURE_2D, gbuffer.tTextures[gbuffer.NORMAL]);

	ocean.causticsMapAnimate();
	ocean.render_water();

	glEnable(GL_CULL_FACE);
	glDisable(GL_BLEND);
	glDisable(GL_POLYGON_OFFSET_EXT);
};

void PNDMAPRENDER::loadHeightmap(const char* filename,float heightscale)
{
	heightmap.initHeightmap(filename,heightscale);
};

void PNDMAPRENDER::renderMap()
{
	terrainShader->bindShader();
	glActiveTexture(GL_TEXTURE0);
	glBindTexture(GL_TEXTURE_2D, heightmap.terr_texture1);
	glActiveTexture(GL_TEXTURE1);
	glBindTexture(GL_TEXTURE_2D, heightmap.terr_texture2);
	glActiveTexture(GL_TEXTURE2);
	glBindTexture(GL_TEXTURE_2D, heightmap.terr_texture3);
	glActiveTexture(GL_TEXTURE3);
	glBindTexture(GL_TEXTURE_2D, heightmap.terr_shore);
	glActiveTexture(GL_TEXTURE4);
	glBindTexture(GL_TEXTURE_2D, heightmap.terr_shore);

	heightmap.renderHeightmap();
};

void PNDMAPRENDER::loadSky(const char*skynear,const char*skyfar,const char*skyright,const char*skyleft,const char*skytop,const char*skybot)
{
	sky.loadSkyBox(skynear,skyfar,skyright,skyleft,skytop,skybot);
};

void PNDMAPRENDER::renderSky()
{
	skyShader->bindShader();
	sky.renderSkyBox();
};