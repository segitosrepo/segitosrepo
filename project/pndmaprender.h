#include "pndheightmap.h"
#include "pndsky.h"
#include "pndwater.h"
#include "pndtexture.h"

#include <GLM\glm.hpp>

extern PNDWATER ocean;
extern PNDSKY sky;
extern PNDHEIGHTMAP heightmap;

#ifndef PNDMAP_H
#define PNDMAP_H

#define MAX_TEXTURES 16
#define NUM_LIGHTS 30
#define NUM_OBJS 1000
#define NUM_ENTITIES 1000

#define MAX_VERTS 30000

class PNDMAPRENDER
{
private:

	static float rendertime;

	void loadPNDMAP();
	void loadPNDWATER();
	void loadPNDLIGHT();

	void drawPNDMAP();
	void drawPNDWATER();
	void drawPNDLIGHT();

public:
	PNDMAPRENDER();
	~PNDMAPRENDER();

	void prepareAttribs();

	void loadHeightmap(const char*,float);
	void renderMap();
	void loadOcean();
	void renderOcean();
	void loadSky(const char*,const char*,const char*,const char*,const char*,const char*);
	void renderSky();
};
#endif

extern PNDMAPRENDER* maprenderer;