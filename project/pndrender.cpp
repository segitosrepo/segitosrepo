#include "pndrender.h"
#include "pndmaprender.h"
#include "dsh.h"
#include "pndshadows.h"


#include <GL/glew.h>
#include <GLM\glm.hpp>
#include <GLM\gtc\matrix_transform.hpp>

PNDSHADOWS shadowmap;

PNDRENDER::PNDRENDER() :rmod(0)
{
	terrainShader = new GLSLProgram("terrainVertex.txt", "terrainFragment.txt", "terrainTessCont.txt");
	waterShader = new GLSLProgram("waterShader.txt");
	skyShader = new GLSLProgram("skyVertex.txt", "skyFragment.txt", "skyTessCont.txt");
	shadowmapShader = new GLSLProgram("shadowmapShader.txt");
}

void PNDRENDER::rendermode()
{
	rmod = !rmod;
}

bool PNDRENDER::init()
{
	if (!terrainShader->initialize()){ return false; }
	terrainShader->linkProgram();

	if (!waterShader->initShaders()){ return false; }
	waterShader->linkProgram();

	if (!skyShader->initialize()){ return false; }
	skyShader->linkProgram();

	if (!shadowmapShader->initShaders()){ return false; }
	shadowmapShader->linkProgram();

	int wnd_width = camera.screenWidth;
	int wnd_height = camera.screenHeight;

	gbuffer.Init(wnd_width, wnd_height);
	shadowmap.createShadowMap(wnd_width, wnd_height);

	glEnable(GL_DEPTH_TEST);
	glDepthFunc(GL_LEQUAL);
	glEnable(GL_SCISSOR_TEST);
	glEnable(GL_BLEND);
	glFrontFace(GL_CCW);
	glEnable(GL_CULL_FACE);
	glHint(GL_PERSPECTIVE_CORRECTION_HINT, GL_NICEST);

	glClearColor(0.1f, 0.1f, 0.1f, 1.0f);

	return true;
}

void PNDRENDER::shadow_pass()
{
	shadowmap.startRenderShadowMap(sky.global_light_direction);
	glCullFace(GL_FRONT);
    heightmap.renderHeightmap();
	glCullFace(GL_BACK);
    shadowmap.endRenderShadowMap();

	glActiveTexture(GL_TEXTURE16);
	glBindTexture(GL_TEXTURE_2D,shadowmap.shadowmap.fbo_texture);

	terrainShader->bindShader();
	terrainShader->sendUniform("shadow_map",16);
	terrainShader->sendUniform4x4("shadow_mvp",glm::value_ptr(shadowmap.light_mvp_matrix));

	waterShader->bindShader();
	waterShader->sendUniform("shadow_map",16);
	waterShader->sendUniform4x4("shadow_mvp",glm::value_ptr(shadowmap.light_mvp_matrix));
};



void PNDRENDER::load()
{
	maprenderer->loadSky("skyback.BMP","skyfront.BMP","skyright.BMP","skyleft.BMP","skytop.BMP","skydown.BMP");
	maprenderer->loadHeightmap("map.BMP",6);
	maprenderer->loadOcean();
}

void PNDRENDER::prepare()
{
	camera.renderMatrix();
	sky.prepareSky();
	maprenderer->prepareAttribs();

	terrainShader->bindShader();
	terrainShader->sendUniform("RENDERMODE",rmod);
	waterShader->bindShader();
	waterShader->sendUniform("RENDERMODE",rmod);

	if(rmod==1)
	{
		glPolygonMode(GL_FRONT_AND_BACK,GL_LINE);
	}
	else if(rmod==0)
	{
		glPolygonMode(GL_FRONT_AND_BACK,GL_FILL);
	};

}

void PNDRENDER::deferred_pass()
{
	if (rmod == 0)
	{
		gbuffer.Bind();

		terrainShader->bindShader();
		terrainShader->sendUniform("gpass", TRUE);
		maprenderer->renderMap();
		terrainShader->sendUniform("gpass", FALSE);

		gbuffer.UnBind();
	};
};

void PNDRENDER::render()
{
	prepare();
	deferred_pass();
	shadow_pass();

	glClear(GL_COLOR_BUFFER_BIT | GL_DEPTH_BUFFER_BIT);
	maprenderer->renderSky();
	maprenderer->renderMap();
	maprenderer->renderOcean();
}

void PNDRENDER::shutdown()
{
	terrainShader->unload();
	waterShader->unload();
	skyShader->unload();
}

void PNDRENDER::resize(unsigned int width,unsigned int height)
{
	ocean.waterRefraction.reshapeFBO(width,height);
	ocean.waterGlobalReflection.reshapeFBO(width,height);
	ocean.waterReflection.reshapeFBO(width,height);
	//shadowmap.shadowmap.reshapeFBO(width,height);

	gbuffer.ResizeTexture(width,height);
}

PNDRENDER renderer;