#include <Windows.h>
#include <iostream>
#include "GLSLProgram.h"

#include "pndcamera.h"

#ifndef PNDRENDER_H
#define PNDRENDER_H

class PNDRENDER
{
public:
	PNDRENDER();
	bool init();
	void render();
	void shutdown();
	void load();
	void resize(unsigned int,unsigned int);
	void rendermode();

private:
	int rmod;
	void deferred_pass();
	void prepare();
	void shadow_pass();
};
#endif

extern PNDRENDER renderer;