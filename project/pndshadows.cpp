#include "pndshadows.h"
#include <GLM\gtc\type_ptr.hpp>
#include <GLM\gtc\matrix_transform.hpp>

unsigned int PNDSHADOWS::splitFrustum(unsigned int i)
{
	assert(i<numSplits);
	C_lin[i] = fNear+(fFar-fNear)*float(i/numSplits);
	C_log[i] = fNear*glm::pow((fFar/fNear),float(i/numSplits));
	C_i[i] = L*C_log[i]+(1-L)*C_lin[i];

	return C_i[i];
};

void PNDSHADOWS::createShadowMap(unsigned int width,unsigned int height)
{
	numSplits=4;
	float L = 0.5;
	float* C_lin = new float[numSplits];
	float* C_log = new float[numSplits];
	float* C_i = new float[numSplits];
	shadowmap.createShadowDepthFBO(width,height);
};

void PNDSHADOWS::startRenderShadowMap(glm::vec3 dir)
{
	if(dir.x == 0 && dir.y == 0 && dir.z == 0){dir = glm::vec3(1.0f,1.0f,1.0f);};

	glm::vec3 up;
	glm::vec3 right = glm::cross(glm::vec3(dir.x,0,dir.y),dir);
	up = glm::cross(right,dir);

	light_model_matrix = glm::mat4x4(1.0f);
	light_view_matrix = glm::lookAt(dir,glm::vec3(0.0f),up);
	light_projection_matrix = glm::ortho(-700.0f, 700.0f, -700.0f, 700.0f,1.0f,3000.0f);

	light_mvp_matrix =light_projection_matrix*light_view_matrix*light_model_matrix;

	shadowmap.bindFBO();

	shadowmapShader->bindShader();
	shadowmapShader->sendUniform4x4("mvp",glm::value_ptr(light_mvp_matrix));
}

void PNDSHADOWS::endRenderShadowMap()
{
	FBOMANAGER::unbindFBO();
}