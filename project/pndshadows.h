#include <GLM\glm.hpp>

#include "fbomanager.h"
#include "GLSLProgram.h"

#include <GL\glew.h>

#ifndef SHADOWS_H
#define SHADOWS_H

class PNDSHADOWS
{
private:
	float fNear;
	float fFar;

	unsigned int numSplits;

	unsigned int mapwidth;
	unsigned int mapheight;

	float L;
	float* C_lin;
	float* C_log;
	float* C_i;

public:
	FBOMANAGER shadowmap;

	void getFrustumInfo(unsigned int,unsigned int);
	unsigned int splitFrustum(unsigned int);

	void createShadowMap(unsigned int,unsigned int);
	void createShadowCubeMap(unsigned int,unsigned int);

	void startRenderShadowMap(glm::vec3);
	void startRenderShadowMap(glm::vec3, GLfloat);
	void endRenderShadowMap();

	glm::mat4x4 light_view_matrix;
    glm::mat4x4 light_projection_matrix;
	glm::mat4x4 light_model_matrix;
	glm::mat4x4 light_mvp_matrix;

};

#endif