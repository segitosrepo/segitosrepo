#include "PNDSKY.h"

#define PI 3.141592

float PNDSKY::skyCoords[72]={
	

   //posX
    20.0,-20.0,20.0,
	20.0,20.0,20.0,
	20.0,20.0,-20.0,
	20.0,-20.0,-20.0,

	//negX
	-20.0,-20.0,-20.0,
	-20.0,20.0,-20.0,
	-20.0,20.0,20.0,
	-20.0,-20.0,20.0,

	20.0,20.0,20.0,
	-20.0,20.0,20.0,
	-20.0,20.0,-20.0,
	20.0,20.0,-20.0, //posY

	-20.0,-20.0,20.0,
    20.0,-20.0,20.0,
	20.0,-20.0,-20.0,
	-20.0,-20.0,-20.0, //negY

	-20.0,-20.0,20.0,
	-20.0,20.0,20.0, //posZ
	20.0,20.0,20.0,
	20.0,-20.0,20.0,

	20.0,-20.0,-20.0,
	20.0,20.0,-20.0,
	-20.0,20.0,-20.0,
	-20.0,-20.0,-20.0 //negZ
};

float PNDSKY::skyTexCoords[48]=
{
	0.0f,0.0f,
	0.0f,1.0f,
	1.0f,1.0f,
	1.0f,0.0f,

	0.0f,0.0f,
	0.0f,1.0f,
	1.0f,1.0f,
	1.0f,0.0f,

	0.0f,1.0f,
	1.0f,1.0f,
	1.0f,0.0f,
	0.0f,0.0f,

	1.0f,0.0f,
	0.0f,0.0f,
	0.0f,1.0f,
	1.0f,1.0f,

	0.0f,0.0f,
	0.0f,1.0f,
	1.0f,1.0f,
	1.0f,0.0f,

	0.0f,0.0f,
	0.0f,1.0f,
	1.0f,1.0f,
	1.0f,0.0f,
};

GLuint PNDSKY::loadSkyTextures(const char* filename)
{
	GLuint tex;
	PNDTEXTURE texture;
	texture.loadteximage(filename);

	glGenTextures(1,&tex);
	glBindTexture (GL_TEXTURE_2D, tex);
	GLfloat largest_supported_anisotropy; 
    glGetFloatv(GL_MAX_TEXTURE_MAX_ANISOTROPY_EXT, &largest_supported_anisotropy); 
    glTexParameterf(GL_TEXTURE_2D, GL_TEXTURE_MAX_ANISOTROPY_EXT, largest_supported_anisotropy);
	glTexParameteri (GL_TEXTURE_2D, GL_TEXTURE_MAG_FILTER, GL_LINEAR_MIPMAP_LINEAR);
	glTexParameteri (GL_TEXTURE_2D, GL_TEXTURE_MIN_FILTER, GL_LINEAR_MIPMAP_LINEAR);
	glTexParameteri (GL_TEXTURE_2D, GL_TEXTURE_WRAP_S, GL_CLAMP_TO_EDGE);
	glTexParameteri (GL_TEXTURE_2D, GL_TEXTURE_WRAP_T, GL_CLAMP_TO_EDGE);
	gluBuild2DMipmaps (GL_TEXTURE_2D, GL_RGB, texture.infoheader.biWidth,
		texture.infoheader.biHeight, GL_BGR, GL_UNSIGNED_BYTE,
		texture.data);
	texture.releaseteximage();
	return tex;
};

float rad = 2000.0f;

void PNDSKY::prepareSky()
{
	global_light_direction.y=rad*glm::sin(vertAngle);
	global_light_direction.x=rad*glm::cos(vertAngle)*glm::cos(horAngle);
	global_light_direction.z=rad*glm::cos(vertAngle)*glm::sin(horAngle);
};

void PNDSKY::controlLight(GLint angle)
{
	switch(angle)
	{
	case 1:
		vertAngle-=PI/180.0;
		break;
	case 2:
		vertAngle+=PI/180.0;
		break;
	case 3:
		horAngle-=PI/180.0;
		break;
	case 4:
		horAngle+=PI/180.0;
		break;
	case 5:
		vertAngle=PI*90.0f/180.0f;
		horAngle=0.0f;
		break;
	default:
		vertAngle=PI*90.0f/180.0f;
		horAngle=0.0f;
		break;
	}
};

void PNDSKY::loadSkyBox(const char* s_near,const char*s_far,const char*s_right,const char*s_left,const char*s_top,const char*s_bot)
{
	vertAngle=PI*45.0f/180.0f;
	horAngle=45.0f;

	global_light_direction.y=rad*glm::sin(vertAngle);
	global_light_direction.x=rad*glm::cos(vertAngle)*glm::cos(horAngle);
	global_light_direction.z=rad*glm::cos(vertAngle)*glm::sin(horAngle);

	skytexture[0]=loadSkyTextures(s_right);
	skytexture[1]=loadSkyTextures(s_left);
	skytexture[2]=loadSkyTextures(s_top);
	skytexture[3]=loadSkyTextures(s_bot);
	skytexture[4]=loadSkyTextures(s_near);
	skytexture[5]=loadSkyTextures(s_far);

	glGenBuffers(1,&vertexBufferID);
	glGenBuffers(1,&texcoordBufferID);
	glGenVertexArrays(1,&arrayBufferID);

	glBindBuffer(GL_ARRAY_BUFFER,vertexBufferID);
	glBufferData(GL_ARRAY_BUFFER,72*sizeof(float),&skyCoords[0],GL_STATIC_DRAW);

	glBindBuffer(GL_ARRAY_BUFFER,texcoordBufferID);
	glBufferData(GL_ARRAY_BUFFER,48*sizeof(float),&skyTexCoords[0],GL_STATIC_DRAW);

	glBindVertexArray(arrayBufferID);

	glEnableVertexAttribArray(0);
	glEnableVertexAttribArray(1);

	glBindBuffer(GL_ARRAY_BUFFER,vertexBufferID);
	glVertexAttribPointer(0,3,GL_FLOAT,GL_FALSE,0,0);

	glBindBuffer(GL_ARRAY_BUFFER,texcoordBufferID);
	glVertexAttribPointer(1,2,GL_FLOAT,GL_FALSE,0,0);

	glBindVertexArray(0);

};

void  PNDSKY::renderSkyBox()
{
	glDepthMask(0); 
	glBindVertexArray(arrayBufferID);
	for(unsigned long i=0;i<6;i++)
	{
		glActiveTexture(GL_TEXTURE0);
		glBindTexture(GL_TEXTURE_2D, skytexture[i]);
		skyShader->sendUniform("skytexture",0);
		glDrawArrays(GL_QUADS,i*4,4);
	}
	glBindVertexArray(GL_NONE);
	glDepthMask(1); 
};