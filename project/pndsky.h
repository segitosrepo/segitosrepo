#include "pndheightmap.h"
#include "pndtexture.h"
#include <GL/glew.h>

#ifndef PNDSKYBOX_H
#define PNDSKYBOX_H

class PNDSKY
{
private:
	GLuint vertexBufferID;
	GLuint vertexIndexBufferID;
	GLuint texcoordBufferID;
	GLuint arrayBufferID;

	GLfloat vertAngle;
	GLfloat horAngle;
public:
	static float skyCoords[72];
	static float skyTexCoords[48];
	static unsigned short cube_indices[];
	GLuint skytexture[6];
	glm::vec3 global_light_direction;
	GLuint loadSkyTextures(const char*);
	void loadSkyBox(const char*,const char*,const char*,const char*,const char*,const char*);
	void renderSkyBox();

	void prepareSky();
	void controlLight(GLint);
};

#endif