#include "pndsystem.h"

PNDSYSTEM::PNDSYSTEM():m_lastTime(0)
{};
PNDSYSTEM::~PNDSYSTEM()
{};

void PNDSYSTEM::getSystemInfo(RECT rect,HWND hWnd)
{
	this->uiWindowRect=rect;

	this->hWnd=hWnd;
};

float PNDSYSTEM::getElapsedSeconds()
{
	float currentTime = float(glutGet(GLUT_ELAPSED_TIME));
	float seconds = float(currentTime - m_lastTime);
	m_lastTime = currentTime;
	return seconds;
}