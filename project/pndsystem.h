#include <Windows.h>

#ifndef PNDSYSTEM_H
#define PNDSYSTEM_H

#define MINOR_VERSION 0
#define MAJOR_VERSION 1

class PNDSYSTEM
{
protected:
	float m_lastTime;
public:
	PNDSYSTEM();
	~PNDSYSTEM();
	void getSystemInfo(RECT,HWND);
	float getElapsedSeconds();
};
#endif