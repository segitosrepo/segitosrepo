#include "pndterrain.h"
#include <GL\glut.h>

void PNDTERRAIN::terrainGridLoad()
{
	PNDTERRAIN::coord=new glm::vec3[PNDTERRAIN::width_x*PNDTERRAIN::width_z];

	for(unsigned int iz=0;iz<PNDTERRAIN::width_z;iz++)
	{
		for(unsigned int ix=0;ix<PNDTERRAIN::width_x;ix++)
		{
			PNDTERRAIN::coord[PNDTERRAIN::width_x*iz+ix].x=ix;
			PNDTERRAIN::coord[PNDTERRAIN::width_x*iz+ix].y=PNDTERRAIN::baseheight;
			PNDTERRAIN::coord[PNDTERRAIN::width_x*iz+ix].z=iz;
		}
	}
};

void PNDTERRAIN::terrainLoad(unsigned int wx,unsigned int wz)
{
	PNDTERRAIN::width_x = wx;
	PNDTERRAIN::width_z = wz;

	PNDTERRAIN::terrainGridLoad();
};

void PNDTERRAIN::terrainLoad(unsigned int wx,unsigned int wz,const char* heightmap,float hscale)
{
	PNDTERRAIN::width_x = wx;
	PNDTERRAIN::width_z = wz;

	PNDTERRAIN::terrainGridLoad();
	PNDHEIGHTMAP::loadHeightmap(heightmap,hscale);

	unsigned int mwx=width_x;
	unsigned int mwz=width_z;

	if(PNDHEIGHTMAP::mapwidth<width_x)
	{
		mwx=mapwidth;
	}

	if(PNDHEIGHTMAP::mapheight<width_z)
	{
		mwz=mapheight;
	}

	for(unsigned int iz=0;iz<mwz;iz++)
	{
		for(unsigned int ix=0;ix<mwx;ix++)
		{
			PNDTERRAIN::coord[mwx*iz+ix].y+=PNDHEIGHTMAP::coords[mwx*iz+ix+1];
		}
	}
};

void PNDTERRAIN::terrainSplit(unsigned int n)
{
	splitWidth=n;

	glm::vec3* splitcoords;

}