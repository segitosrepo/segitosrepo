#include "pndheightmap.h"
#include <GLM\glm.hpp>

#ifndef PNDTERRAIN_H
#define PNDTERRAIN_H

class PNDTERRAIN:public PNDHEIGHTMAP
{
public:
	void terrainLoad(unsigned int,unsigned int);
	void terrainLoad(unsigned int,unsigned int,const char*,float);
	void terrainRender();
private:
	unsigned int splitWidth;

	unsigned int width_x;
	unsigned int width_z;
	unsigned int baseheight;

	void terrainGridLoad();
	void terrainSplit(unsigned int);

	glm::vec3* coord;
	GLuint* index;
};

#endif