#include "pndtexture.h"

bool PNDTEXTURE::loadteximage(const char* filename)
{
	FILE *fp;
    long image_size;

	releaseteximage();

	fopen_s(&fp,filename, "rb");
    if (fp == NULL) return false;
	fread (&fileheader, sizeof(BITMAPFILEHEADER), 1, fp);
	fread (&infoheader,sizeof(BITMAPINFOHEADER), 1, fp);
	if (infoheader.biBitCount != 24)
	{
		memset (&fileheader, 0, sizeof(BITMAPFILEHEADER));
		memset (&infoheader, 0, sizeof(BITMAPINFOHEADER));
		fclose (fp);
		return (false);
	}
	image_size = infoheader.biWidth * infoheader.biHeight * 3;
	data = new unsigned char[image_size+1];
	fread (data, image_size, 1, fp);
	fclose (fp);
	return (true);
}

bool PNDTEXTURE::loadteximagef(const char* filename)
{
	FILE *fp;
    long image_size;

	releaseteximage();

	fopen_s(&fp,filename, "rb");
    if (fp == NULL) return false;
	fread (&fileheader, sizeof(BITMAPFILEHEADER), 1, fp);
	fread (&infoheader,sizeof(BITMAPINFOHEADER), 1, fp);
	if (infoheader.biBitCount != 24)
	{
		memset (&fileheader, 0, sizeof(BITMAPFILEHEADER));
		memset (&infoheader, 0, sizeof(BITMAPINFOHEADER));
		fclose (fp);
		return (false);
	}
	image_size = infoheader.biWidth * infoheader.biHeight * 3;
	fdata = new float[image_size+1];
	fread (fdata, image_size, 1, fp);
	fclose (fp);
	return (true);
}

void PNDTEXTURE::releaseteximage()
{
if (data != NULL)
{
delete [] data;
data = NULL;
memset (&fileheader, 0, sizeof(fileheader));
memset (&infoheader, 0, sizeof(infoheader));
}

if (fdata != NULL)
{
delete [] fdata;
fdata = NULL;
memset (&fileheader, 0, sizeof(fileheader));
memset (&infoheader, 0, sizeof(infoheader));
}

}

unsigned int PNDTEXTURE::load_texture(const char* filename)
{
	GLuint tex;
	PNDTEXTURE texture;
	texture.loadteximage(filename);

	glGenTextures(1,&tex);
	glBindTexture (GL_TEXTURE_2D, tex);
	GLfloat largest_supported_anisotropy; 
    glGetFloatv(GL_MAX_TEXTURE_MAX_ANISOTROPY_EXT, &largest_supported_anisotropy); 
    glTexParameterf(GL_TEXTURE_2D, GL_TEXTURE_MAX_ANISOTROPY_EXT, largest_supported_anisotropy);
	glTexParameteri (GL_TEXTURE_2D, GL_TEXTURE_MAG_FILTER, GL_LINEAR);
	glTexParameteri (GL_TEXTURE_2D, GL_TEXTURE_MIN_FILTER, GL_LINEAR_MIPMAP_LINEAR);
	glTexParameteri (GL_TEXTURE_2D, GL_TEXTURE_WRAP_S, GL_REPEAT);
	glTexParameteri (GL_TEXTURE_2D, GL_TEXTURE_WRAP_T, GL_REPEAT);
	gluBuild2DMipmaps (GL_TEXTURE_2D, GL_RGB, texture.infoheader.biWidth,
		texture.infoheader.biHeight, GL_BGR, GL_UNSIGNED_BYTE,
		texture.data);
	texture.releaseteximage();
	return tex;
}