#include <Windows.h>
#include <iostream>

#include <GL\glew.h>
#include <GL/glut.h>

#ifndef PNDTEXTURE_H
#define PNDTEXTURE_H

class PNDTEXTURE
{
public:
	BITMAPFILEHEADER fileheader;
	BITMAPINFOHEADER infoheader;

	unsigned char* data;
	float* fdata;

	PNDTEXTURE():data(NULL),fdata(NULL)
	{
		memset(&fileheader,0,(sizeof(BITMAPFILEHEADER)));
		memset(&infoheader,0,(sizeof(BITMAPINFOHEADER)));
	}

	bool loadteximage(const char*);
	bool loadteximagef(const char*);

	static unsigned int load_texture(const char* filename);

	void releaseteximage();
};
#endif