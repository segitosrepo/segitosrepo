#include "pndwater.h"
#include <iostream>
#include <fstream>

void PNDWATER::loadWater(float width,float height)
{
	for(int rows=0;rows<height;rows++)
		for(int cols=0;cols<width;cols++)
		{
			watercoords.push_back(cols);
	        watercoords.push_back(0);
			watercoords.push_back(rows);
		}

	for(int row=0;row<height-1;row++)
	{
		for(int col=0;col<width-1;col++)
		{
			waterindex.push_back(row*width+col);
			waterindex.push_back((row+1)*width+col);
			waterindex.push_back(row*width+col+1);

			waterindex.push_back(row*width+col+1);
            waterindex.push_back((row+1)*width+col);
			waterindex.push_back((row+1)*width+col+1);
		}
	}

	indexRange=6*width;
	indexRows=height-1;
};

GLuint PNDWATER::causticsTexGen(const char* filename)
{
	GLuint tex;
	PNDTEXTURE heighttexture;
	heighttexture.loadteximage(filename);

	glGenTextures(1,&tex);
	glBindTexture (GL_TEXTURE_2D, tex);
	GLfloat largest_supported_anisotropy; 
    glGetFloatv(GL_MAX_TEXTURE_MAX_ANISOTROPY_EXT, &largest_supported_anisotropy);
    glTexParameterf(GL_TEXTURE_2D, GL_TEXTURE_MAX_ANISOTROPY_EXT, largest_supported_anisotropy);
	glTexParameteri (GL_TEXTURE_2D, GL_TEXTURE_MAG_FILTER, GL_LINEAR);
	glTexParameteri (GL_TEXTURE_2D, GL_TEXTURE_MIN_FILTER, GL_LINEAR);
	glTexParameteri (GL_TEXTURE_2D, GL_TEXTURE_WRAP_S, GL_REPEAT);
	glTexParameteri (GL_TEXTURE_2D, GL_TEXTURE_WRAP_T, GL_REPEAT);
	glTexImage2D (GL_TEXTURE_2D,0, GL_RGB, heighttexture.infoheader.biWidth,
		heighttexture.infoheader.biHeight,0, GL_BGR, GL_UNSIGNED_BYTE,
		heighttexture.data);
	heighttexture.releaseteximage();
	return tex;
}

PNDTEXTURE PNDWATER::animTexGen(const char* filename)
{
	PNDTEXTURE texture;
	texture.loadteximage(filename);
	return texture; 
}

struct sCh{
	char cNo[3];
};

void PNDWATER::causticsMapGen()
{
	std::ofstream fp;
	fp.open("textures/output.txt");

	char* path ="textures/water/lake/BMP/save.";

	std::vector<sCh>frame_no;
	char* extension = ".BMP";
	char** full_path;
	full_path=new char*[MAX_FRAMES];

	int* n = new int[MAX_FRAMES];

	for(int i=0;i<MAX_FRAMES;i++)
	{
		full_path[i]=new char[std::strlen(path)+std::strlen(extension)+4];
		char *buffer=new char[3];
		n[i]=i+1;
		sprintf(buffer, "%d", n[i]);

		sCh no;
		no.cNo[0]=buffer[0];
		no.cNo[1]=buffer[1];
		no.cNo[2]=buffer[2];

		frame_no.push_back(no);
		delete[] buffer;

		std::strcpy(full_path[i],path);
	    std::strcat(full_path[i],frame_no[i].cNo);
		std::strcat(full_path[i],extension);
	    fp<<full_path[i]<<"::";

		caustics_anim_map[i]=animTexGen(full_path[i]);
		fp<<&caustics_anim_map[i]<<"\n";
	}
	caustics_map=causticsTexGen(full_path[0]);
	fp.close();
}

int frame = 0;
int lasttime = 0;
int dtime = 0;

void PNDWATER::causticsMapAnimate()
{
	int currenttime = glutGet(GLUT_ELAPSED_TIME);
	dtime+=(currenttime - lasttime);
	lasttime = currenttime;

	int animation_speed = 35;

	if(dtime>animation_speed)
	{
		frame++;
		dtime = 0;
	}

	glBindTexture(GL_TEXTURE_2D,caustics_map);
	glTexSubImage2D(GL_TEXTURE_2D,0,0,0,caustics_anim_map[frame].infoheader.biWidth,caustics_anim_map[frame].infoheader.biHeight,
		GL_BGR,GL_UNSIGNED_BYTE,caustics_anim_map[frame].data);

	if(frame>MAX_FRAMES-1)
		frame = 0;
}

void PNDWATER::init_water(unsigned int mapwidth,unsigned int mapheight)
{
	waterShader->bindShader();
	waterlevel=1.0f;
	loadWater(mapwidth,mapheight);

	normal_map = PNDTEXTURE::load_texture("textures/normal.BMP");
	foam = PNDTEXTURE::load_texture("textures/foam.BMP");
	causticsMapGen();

	glGenBuffers(1,&waterCoordBufferID);
	glGenBuffers(1,&waterIndexBufferID);

	glGenVertexArrays(1,&waterVertArrayBuffer);

	glBindBuffer(GL_ELEMENT_ARRAY_BUFFER,waterIndexBufferID);
	glBufferData(GL_ELEMENT_ARRAY_BUFFER,sizeof(GLuint)*waterindex.size(),&(waterindex[0]),GL_DYNAMIC_DRAW);

	glBindBuffer(GL_ARRAY_BUFFER,waterCoordBufferID);
	glBufferData(GL_ARRAY_BUFFER,sizeof(GLfloat)*watercoords.size(),&(watercoords[0]),GL_DYNAMIC_DRAW);

	glBindVertexArray(waterVertArrayBuffer);

	glEnableVertexAttribArray(0);

	glBindBuffer(GL_ARRAY_BUFFER,waterCoordBufferID);
	glVertexAttribPointer(0,3,GL_FLOAT,GL_FALSE,0,0);

	glBindBuffer(GL_ELEMENT_ARRAY_BUFFER,waterIndexBufferID);
	
	glBindVertexArray(0);
}

void PNDWATER::render_water()
{
    glBindVertexArray(waterVertArrayBuffer);
	for(unsigned long indRow=0;indRow<indexRows;indRow++)
	{glDrawElements(GL_TRIANGLES,indexRange,GL_UNSIGNED_INT,(void*)(sizeof(unsigned long)*indexRange*indRow));}
	glBindVertexArray(GL_NONE);
};

void PNDWATER::incWaterHeight()
{
	waterlevel+=0.05;
};
void PNDWATER::decrWaterHeight()
{
	waterlevel-=0.05;
};