#include <Windows.h>
#include <iostream>
#include <vector>
#include <math.h>

#include <GL/glew.h>
#include <GL/glut.h>

#include "pndtexture.h"
#include "fbomanager.h"
#include "GLSLProgram.h"
#include "dsh.h"

#ifndef PNDWATER_H
#define PNDWATER_H

#define MAX_FRAMES 32

class PNDWATER
{
public:
	std::vector<float> watercoords;
	std::vector<unsigned int> waterindex;
	std::vector<float> waterdepth;

	void loadWater(float,float);
	void init_water(unsigned int,unsigned int);
	void render_water();

	GLuint causticsTexGen(const char*);
	PNDTEXTURE animTexGen(const char*);
	void causticsMapGen();
	void causticsMapAnimate();

	PNDTEXTURE caustics_anim_map[MAX_FRAMES];
	GLuint caustics_map;
	GLuint normal_map;
	GLuint foam;

	GLuint waterCoordBufferID;
	GLuint waterIndexBufferID;
	GLuint waterDepthBufferID;
	GLuint waterVertArrayBuffer;

	FBOMANAGER waterReflection;
	FBOMANAGER waterGlobalReflection;
	FBOMANAGER waterRefraction;

	float waterlevel;
	void decrWaterHeight();
	void incWaterHeight();

	unsigned int indexRange;
	unsigned int indexRows;

};
#endif